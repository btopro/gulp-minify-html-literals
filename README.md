# Gulp Minify HTML Literals

A simple [Gulp] plugin wrapping [minify-html-literals].

Minify HTML in your ES2015 (or later) string literals, e.g. when using [polymer] or [lit-element] or custom [webcomponents] or other JavaScript files that uses ECMASCRIPT string literals with HTML.

## Example Usage

Example `gulpfile.js`:

```javascript
const {
  src,
  dest,
  parallel,
  series
} = require('gulp');

const gulpMinifyJsTemplate = require('gulp-minify-js-template');

const distWidgets = () =>
  src([`./my-widgets/**/*.js`])
  .pipe(minifyJSTemplate()) //or with options minifyJSTemplate({...})
  .pipe(dest('./dist/js/widgets/'));

module.exports = {

  default: distWidgets

};

```

For the actual options, see [minify-html-literals].

### License

*Gulp Minify HTML Literals* is [MIT] licensed.

[minify-html-literals]: https://www.npmjs.com/package/minify-html-literals
[through2]: https://www.npmjs.com/package/through2
[polymer]: https://polymer-library.polymer-project.org/3.0/docs/about_30
[lit-element]: https://lit-element.polymer-project.org/
[webcomponents]: https://www.webcomponents.org/
[gulp]: https://gulpjs.com/
[mit]: https://gitlab.com/bttg_/gulp-minify-html-literals/-/raw/master/LICENSE
