const through = require('through2');
const minifyHTMLLiterals = require('minify-html-literals').minifyHTMLLiterals;

module.exports = options => through.obj((vinylFile, encoding, callback) => {

  const transformedFile = vinylFile.clone();
  const bufferToString = transformedFile.contents.toString('utf-8');
  const result = minifyHTMLLiterals(bufferToString, options);

  if (result !== null)
    transformedFile.contents = Buffer.from(result.code);

  callback(null, transformedFile);

});